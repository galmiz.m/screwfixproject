package org.mytests.tests.states;

import com.epam.jdi.light.elements.composite.WebPage;
import io.qameta.allure.Step;
import org.mytests.tests.test.data.ReadProperties;
import org.mytests.uiobjects.example.entities.User;
import org.mytests.uiobjects.example.site.SiteScrewfix;
import org.mytests.uiobjects.example.site.pages.BasketPage;
import org.mytests.uiobjects.example.site.pages.LoginPage;


import static org.mytests.uiobjects.example.site.SiteScrewfix.*;

/**
 * Created by Roman_Iovlev on 3/1/2018.
 */
public class States {
    public static void checkPopUp() {
        try {
            if (popUpAcceptButton.isDisplayed()) popUpAcceptButton.click();
        } catch (org.openqa.selenium.NoSuchElementException e) {
        }
    }

    public static void enterStaticUser() throws Exception {
        SiteScrewfix.loginPage.open();
        Thread.sleep(2000);
        States.checkPopUp();
        LoginPage.emailAddress.setValue(ReadProperties.getData("emailAddress"));
        LoginPage.pass.setValue(ReadProperties.getData("pass"));
        LoginPage.signInBtn.click();
    }

    public static void checkBasketIsEmpty() throws InterruptedException {
        if (basketNumberItems.isDisplayed()){
            basketNumberItems.click();
            while (basketNumberItems.isDisplayed()){
                Thread.sleep(1000);
                BasketPage.removeButton.click();
                Thread.sleep(1000);
            }
        }
    }
}
//            while (basketNumberItems.isDisplayed()){
//                Thread.sleep(1000);
//                BasketPage.removeButton.click();
//                Thread.sleep(1000);
//            }
//            do {
//                    Thread.sleep(1000);
//                    BasketPage.removeButton.click();
//                    Thread.sleep(1000);
//                    } while (basketNumberItems.isDisplayed());
//    public static boolean isDisplayed2(){
//        try {
//
//            if (secondPopUp.isDisplayed()) return true;
//        }
//        catch (org.openqa.selenium.NoSuchElementException e) {
//            return false;
//        }
//        return false;
//    }
//    public static void checkPopUp2() {
//        try {
//            if (secondPopUp.isDisplayed()) secondPopUp.click();
//        }
//        catch (org.openqa.selenium.NoSuchElementException e) {
//        }
//
//    }
//    }
//    private static void onSite() {
//        if (!WebPage.getUrl().contains("https://jdi-testing.github.io/jdi-light/"))
//            homePage.open();
//    }
//    @Step
//    public static void shouldBeLoggedIn() {
//        onSite();
//        if (!userName.isDisplayed())
//            login();
//    }
//    @Step
//    public static void login() {
//        userIcon.click();
//        loginForm.submit(new User(), "enter");
//    }
//    @Step
//    public static void shouldBeLoggedOut() {
//        onSite();
//        if (userName.isDisplayed())
//            logout();
//        if (loginForm.isDisplayed())
//            userIcon.click();
//    }
//    @Step
//    public static void logout() {
//        if (!logout.isDisplayed())
//            userIcon.click();
//        logout.click();
//    }

