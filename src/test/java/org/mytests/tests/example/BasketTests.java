package org.mytests.tests.example;

import org.mytests.tests.TestsInit;
import org.mytests.tests.states.States;
import org.mytests.tests.test.data.ReadProperties;
import org.mytests.uiobjects.example.site.SiteScrewfix;
import org.mytests.uiobjects.example.site.pages.BasketPage;
import org.mytests.uiobjects.example.site.pages.HomePage;
import org.mytests.uiobjects.example.site.pages.SearchResultPage;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

public class BasketTests implements TestsInit {

    @Test
    public void addProductToBasket() throws Exception {
        States.enterStaticUser();
        States.checkBasketIsEmpty();
        SiteScrewfix.homePage.open();
        HomePage.searchField.setValue(ReadProperties.getData("firstItemToSearch"));
        HomePage.searchButton.click();
        SearchResultPage.deliverButton.click();
        SearchResultPage.continueButton.click();
        HomePage.searchField.setValue(ReadProperties.getData("secondItemToSearch"));
        HomePage.searchButton.click();
        SearchResultPage.clickAndCollectButton.click();
//        SearchResultPage.findStoresField.setValue(ReadProperties.getData("areaCode"));
//        SearchResultPage.searchStoresButton.click();
//        SearchResultPage.addToBasketStoreButton.click();
        SearchResultPage.checkoutNowButton.click();
        assertEquals(BasketPage.firstItemType.getAttribute("alt"), "Screwfix items for Click and Collect");
        assertEquals(BasketPage.firstItemPrice.getText(), ReadProperties.getData("firstItemPrice"));
        assertEquals(BasketPage.secondItemType.getAttribute("alt"), "Screwfix items for delivery ");
        assertEquals(BasketPage.secondItemPrice.getText(), ReadProperties.getData("secondItemPrice"));

        assertEquals(BasketPage.totalItemsPriceIncVAT.getText(), ReadProperties.getData("totalItemPriceIncVAT"));
        assertEquals(BasketPage.totalItemsPriceExVAT.getText(), ReadProperties.getData("totalItemPriceExVAT"));


    }
}
