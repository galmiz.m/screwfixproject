package org.mytests.tests.example;


import com.epam.jdi.light.elements.pageobjects.annotations.Frame;
import org.mytests.tests.TestsInit;
import org.mytests.tests.states.States;
import org.mytests.tests.test.data.ReadProperties;
import org.mytests.uiobjects.example.site.SiteScrewfix;
import org.mytests.uiobjects.example.site.pages.HomePage;
import org.mytests.uiobjects.example.site.pages.LoginPage;
import org.openqa.selenium.By;
import org.openqa.selenium.devtools.v96.page.Page;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class LoginTests implements TestsInit {
//    @BeforeTest
//    public void popUpCheck(){
//        States.checkPopUp();
//    }

    @Test
    public void loginTest() throws Exception {
        SiteScrewfix.loginPage.open();
        Thread.sleep(4000);
        States.checkPopUp();
       LoginPage.emailAddress.setValue(ReadProperties.getData("emailAddress"));
       LoginPage.pass.setValue(ReadProperties.getData("pass"));
       LoginPage.signInBtn.click();
       Thread.sleep(2000);
       assertTrue(LoginPage.welcomeMessage.isDisplayed());
       LoginPage.continueShoppingButton.click();
       assertTrue(SiteScrewfix.homePage.isOpened());

    }

    @Test(priority = 1)
    public void logout() throws Exception {
        HomePage.myAccountButton.click();
        HomePage.signOutButton.click();
        assertTrue(HomePage.getUrl().contains(ReadProperties.getData("signOutUrl")));
    }


//    @Test
//    public void openLoginPageTest() {
//        SiteScrewfix.loginPage.open();
//    //    SiteScrewfix.loginPage.driver().switchTo().
//     //   SiteScrewfix.loginPage.driver().switchTo().parentFrame();
//        //SiteScrewfix.loginPage.driver().switchTo().frame(SiteScrewfix.popUpAcceptButton);
////        States.checkPopUp();
////        States.checkPopUp2();
//
//        SiteScrewfix. popUpAcceptButton.click();
//        SiteScrewfix.loginPage.checkOpened();
//        LoginPage.registerButton.click();
//
//        assertEquals(1,1);
//
//    }
//  @Test
//    public void openHomePageTest(){
//        SiteScrewfix.homePage.open();
//        SiteScrewfix.homePage.checkOpened();
//
//    }
}
