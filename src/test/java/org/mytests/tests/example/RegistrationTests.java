package org.mytests.tests.example;

import com.epam.jdi.light.elements.pageobjects.annotations.Url;
import org.mytests.tests.TestsInit;
import org.mytests.tests.states.States;
import org.mytests.uiobjects.example.entities.Defaults;
import org.mytests.uiobjects.example.site.SiteScrewfix;
import org.mytests.uiobjects.example.site.pages.RegistrationCompletePage;
import org.mytests.uiobjects.example.site.pages.RegistrationFormPage;
import org.mytests.uiobjects.example.site.pages.RegistrationPage;
import org.mytests.uiobjects.example.site.sections.RegistationForm;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;


public class RegistrationTests implements TestsInit {
    @BeforeTest
    public static void openPage(){
        SiteScrewfix.registrationPage.shouldBeOpened();
        States.checkPopUp();

    }


    @Test
    public static void inputDataToForm() throws InterruptedException {
        RegistrationPage.emailAddressInput.setValue(Defaults.DEFAULT_REGISTRATIONS.email);
        Thread.sleep(10000);
        States.checkPopUp();
        RegistrationPage.continueRegistrationButton.click();
        Thread.sleep(5000);

        RegistrationPage.newTitle.select(Defaults.DEFAULT_REGISTRATIONS.newTitle);
        RegistrationPage.firstName.setValue(Defaults.DEFAULT_REGISTRATIONS.firstName);
        RegistrationPage.lastName.setValue(Defaults.DEFAULT_REGISTRATIONS.lastName);
        RegistrationPage.profession.select(2);
        RegistrationPage.contactSearchPostcode.setValue(Defaults.DEFAULT_REGISTRATIONS.contactSearchPostcode);
        RegistrationPage.findAddressButton.click();
        Thread.sleep(10000);
        RegistrationPage.contactAddressResultSelect.select(2);
        RegistrationPage.password.setValue(Defaults.DEFAULT_REGISTRATIONS.password);
        RegistrationPage.retypePassword.setValue(Defaults.DEFAULT_REGISTRATIONS.retypePassword);
        assertTrue(RegistrationPage.registerNowButton.isDisplayed());
        RegistrationPage.registerNowButton.doubleClick();
        Thread.sleep(10000);
        assertTrue(RegistrationCompletePage.greenIcon.isDisplayed());
        assertTrue(SiteScrewfix.registrationCompletePage.isOpened());




    }


//    @Test
//    public static void inputDataToForm() throws InterruptedException {
//        RegistrationPage.emailAddressInput.sendKeys("qwe@examle.com");
//        Thread.sleep(10000);
//        States.checkPopUp();
//        RegistrationPage.continueRegistrationButton.click();
//        Thread.sleep(10000);
//        RegistrationPage.newTitle.select("Mr");
//
//        RegistrationFormPage.registationForm.submit(Defaults.DEFAULT_REGISTRATIONS);
//        assertEquals(1, 1);
//
//    }
}
