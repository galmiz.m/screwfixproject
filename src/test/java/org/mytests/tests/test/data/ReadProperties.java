package org.mytests.tests.test.data;

import java.io.*;
import java.util.Properties;

import static org.bouncycastle.asn1.cms.CMSObjectIdentifiers.data;

public class ReadProperties {
   static Properties properties;
   public static ReadProperties prop = new ReadProperties();

    public ReadProperties() {

        try {
            FileInputStream Locator = new FileInputStream("src/test/resources/data.properties");
            properties = new Properties();
            properties.load(Locator);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static String getData(String ElementName) throws Exception {
        // Read value using the logical name as Key
        String data = properties.getProperty(ElementName);
        return data;
    }
}







