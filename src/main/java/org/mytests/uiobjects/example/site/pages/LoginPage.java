package org.mytests.uiobjects.example.site.pages;

import com.epam.jdi.light.elements.composite.WebPage;
import com.epam.jdi.light.elements.pageobjects.annotations.FindBy;
import com.epam.jdi.light.elements.pageobjects.annotations.Frame;
import com.epam.jdi.light.elements.pageobjects.annotations.Title;
import com.epam.jdi.light.elements.pageobjects.annotations.Url;
import com.epam.jdi.light.ui.html.elements.common.TextField;
import org.openqa.selenium.WebElement;


import java.util.Properties;

@Url("/login/") @Title("Login Page") @Frame("pop-frame")
public class LoginPage extends WebPage {
    @FindBy(xpath = "//button[@value='Register now']") public static WebElement registerButton;

    @FindBy(id = "email-existing") public static TextField emailAddress;
    @FindBy(id = "password-field") public static TextField pass;
    @FindBy(id = "sign-in-btn") public static WebElement signInBtn;

    @FindBy(id = "log_in_confirm_page_welcome_back_message") public static WebElement welcomeMessage;
    @FindBy(xpath = "//a[@title='Continue shopping']") public static WebElement continueShoppingButton;
}
