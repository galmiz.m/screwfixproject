package org.mytests.uiobjects.example.site.pages;

import com.epam.jdi.light.elements.complex.dropdown.DropdownSelect;
import com.epam.jdi.light.elements.composite.WebPage;
import com.epam.jdi.light.elements.pageobjects.annotations.FindBy;
import com.epam.jdi.light.elements.pageobjects.annotations.Title;
import com.epam.jdi.light.elements.pageobjects.annotations.Url;
import com.epam.jdi.light.elements.pageobjects.annotations.locators.UI;
import com.epam.jdi.light.ui.html.elements.common.Button;
import com.epam.jdi.light.ui.html.elements.common.Checkbox;
import com.epam.jdi.light.ui.html.elements.common.TextField;
import org.openqa.selenium.WebElement;

@Url("/registrationpage/?destination=&loginPagePath=%2Floginpage%2F&_requestid=227105") @Title("Registration Page")
public class RegistrationPage extends WebPage {

    @FindBy(xpath = "//input[@id ='email-input']") public static TextField emailAddressInput;
    @FindBy(id = "continueRegistrationButton") public static WebElement continueRegistrationButton;

//    TextField firstName, lastName, contactSearchPostcode, password, retypePassword;
//
//    DropdownSelect title, profession;

    @FindBy(id = "newTitle") public static DropdownSelect newTitle;
    @FindBy(id = "firstName") public static TextField firstName;
    @FindBy(id = "lastName") public static TextField lastName;
    @FindBy(id = "profession") public static DropdownSelect profession;
    @FindBy(id = "contact-search-postcode") public static TextField contactSearchPostcode;
    @FindBy(xpath = "//button[@data-value = 'find address']") public static WebElement findAddressButton;
    @FindBy(id = "contact-address-result-select") public static DropdownSelect contactAddressResultSelect;

    @FindBy(id = "password") public static TextField password;
    @FindBy(id = "retypePassword") public static TextField retypePassword;

    @FindBy(id = "registerNowButton") public static Button registerNowButton;





//    @FindBy(id = "newTitle") public static WebElement newTitle;
//    @FindBy(id = "firstName") public static WebElement firstName;
//    @FindBy(id = "lastName") public static WebElement lastName;
//    @FindBy(id = "profession") public static WebElement profession;
//    @FindBy(id = "contact-search-postcode") public static WebElement contactSearchPostcode;
//    @FindBy(id = "contact-address-result-select") public static WebElement contactAddressResultSelect;
//
//    @FindBy(id = "password") public static WebElement password;
//    @FindBy(id = "retypePassword") public static WebElement retypePassword;
//
//    @FindBy(id = "registerNowButton") public static WebElement registerNowButton;


}
