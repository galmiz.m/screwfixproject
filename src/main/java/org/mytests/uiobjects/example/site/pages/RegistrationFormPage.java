package org.mytests.uiobjects.example.site.pages;

import com.epam.jdi.light.elements.composite.WebPage;
import com.epam.jdi.light.elements.pageobjects.annotations.FindBy;
import com.epam.jdi.light.elements.pageobjects.annotations.Title;
import com.epam.jdi.light.elements.pageobjects.annotations.Url;
import com.epam.jdi.light.elements.pageobjects.annotations.locators.UI;
import org.mytests.uiobjects.example.site.sections.RegistationForm;

@Url("/registrationpage/?destination=&loginPagePath=%2Floginpage%2F&_requestid=227105") @Title("Registration Page")
public class RegistrationFormPage extends WebPage {
    @FindBy(className = "lg-12 md-18 sm-24 cols js--address-with-country") public static RegistationForm registationForm;
}
