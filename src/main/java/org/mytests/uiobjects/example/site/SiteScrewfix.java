package org.mytests.uiobjects.example.site;

import com.epam.jdi.light.elements.pageobjects.annotations.FindBy;
import com.epam.jdi.light.elements.pageobjects.annotations.Frame;
import org.mytests.uiobjects.example.site.pages.*;
import org.openqa.selenium.WebElement;

//@JSite("https://www.screwfix.com/")
public class SiteScrewfix {
    public static HomePage homePage;
    public static LoginPage loginPage;
    public static RegistrationPage registrationPage;
    public static SearchResultPage searchResultPage;
    public static BasketPage basketPage;
    public static RegistrationCompletePage registrationCompletePage;
//    public static RegistrationFormPage registrationFormPage;

    @FindBy( xpath = "//a[@id='header_link_sign_in']") public static WebElement loginButton;

    @FindBy(xpath = "//div[@class='truste_overlay']") public static WebElement popUpField;
    @Frame(".truste_popframe")@FindBy(xpath = "//a[@class='call']") public static WebElement popUpAcceptButton;
    @FindBy(xpath = "//button[@class='lb-btn-cancel']") public static WebElement secondPopUp;

    @FindBy(xpath = "//iframe[contains(@id,'pop-frame')]") public static WebElement popUpFrame;

    @FindBy(xpath = "//i[@class = 'icon-qty']") public static WebElement basketNumberItems;



}
