package org.mytests.uiobjects.example.site.pages;

import com.epam.jdi.light.elements.composite.WebPage;
import com.epam.jdi.light.elements.pageobjects.annotations.FindBy;
import com.epam.jdi.light.ui.html.elements.common.TextField;
import org.openqa.selenium.WebElement;

import java.awt.*;

public class SearchResultPage extends WebPage {

    @FindBy(id = "product_add_to_trolley_image") public static WebElement deliverButton;
    @FindBy(id = "continue_button_btn") public static WebElement continueButton;
    @FindBy(xpath = "//button[contains(@id, 'add_for_collection_button_')]") public static WebElement clickAndCollectButton;
    @FindBy(id = "search_branch_textbox") public static TextField findStoresField;
    @FindBy(id = "search_branch_button") public static WebElement searchStoresButton;
    @FindBy(id = "add_for_collection_button_1") public static WebElement addToBasketStoreButton;
    @FindBy(id = "checkout_now_btn") public static WebElement checkoutNowButton;

}
