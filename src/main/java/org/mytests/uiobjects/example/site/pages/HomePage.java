package org.mytests.uiobjects.example.site.pages;

import com.epam.jdi.light.elements.composite.WebPage;
import com.epam.jdi.light.elements.pageobjects.annotations.FindBy;
import com.epam.jdi.light.elements.pageobjects.annotations.Frame;
import com.epam.jdi.light.elements.pageobjects.annotations.Title;
import com.epam.jdi.light.elements.pageobjects.annotations.Url;
import com.epam.jdi.light.elements.pageobjects.annotations.locators.Css;
import com.epam.jdi.light.elements.pageobjects.annotations.locators.UI;
import com.epam.jdi.light.ui.html.elements.common.TextField;
import org.openqa.selenium.WebElement;

@Url("https://www.screwfix.com/") @Title("Home Page")
public class HomePage extends WebPage {
//    @FindBy( id= "header_link_sign_in") public static WebElement loginButton;
// @UI(".acc")public static WebElement loginButton;

    @FindBy(id = "header_my_account_link") public static WebElement myAccountButton;
    @FindBy(id = "header_link_sign_out") public static WebElement signOutButton;

    @FindBy(id = "mainSearch-input") public static TextField searchField;
    @FindBy(id = "search_submit_button") public static WebElement searchButton;
}
