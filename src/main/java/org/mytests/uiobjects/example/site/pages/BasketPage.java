package org.mytests.uiobjects.example.site.pages;

import com.epam.jdi.light.elements.composite.WebPage;
import com.epam.jdi.light.elements.pageobjects.annotations.FindBy;
import com.epam.jdi.light.elements.pageobjects.annotations.Url;
import org.openqa.selenium.WebElement;

@Url("/jsp/trolley/trolleyPage.jsp")
public class BasketPage extends WebPage {
    @FindBy(xpath = "//a[@id = \"trolley_page_product_remove_item_link_1\"]") public static WebElement removeButton;
    @FindBy(xpath = "//div[2]//span[@class ='item-list__title--type']/img") public static WebElement firstItemType;
    @FindBy(id = "CPC_trolley_page_product_total_price_1") public static WebElement firstItemPrice;
    @FindBy(xpath = "//div[3]//span[@class ='item-list__title--type']/img") public static WebElement secondItemType;
    @FindBy(id = "trolley_page_product_total_price_1") public static WebElement secondItemPrice;

    @FindBy(id = "trolley_page_grand_total") public static WebElement totalItemsPriceIncVAT;
    @FindBy(id = "trolley_page_grand_total_ex_vat") public static WebElement totalItemsPriceExVAT;


}
