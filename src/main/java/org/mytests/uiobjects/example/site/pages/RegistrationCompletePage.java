package org.mytests.uiobjects.example.site.pages;

import com.epam.jdi.light.elements.composite.WebPage;
import com.epam.jdi.light.elements.pageobjects.annotations.FindBy;
import com.epam.jdi.light.elements.pageobjects.annotations.Url;
import org.openqa.selenium.WebElement;

@Url("/jsp/registerSuccess.jsp")
public class RegistrationCompletePage extends WebPage {

    @FindBy(xpath = "//span[@class='icon-ok icon--round icon--big']") public static WebElement greenIcon;
}
