package org.mytests.uiobjects.example.entities;

import java.util.Random;


public class Defaults {
    static Random random = new Random();
    public static User DEFAULT_USER = new User();

    public static Registrations DEFAULT_REGISTRATIONS = new Registrations().set(c-> {
        c.email = "user" + random.nextInt(10000000) + "@example.com";
        c.newTitle = "Mr";
        c.firstName = "Max"; c.lastName = "Xam"; c.profession = "\n" +
                "\t\t\t\t\t\t\t\t\tBuilder/Multi Trade\n" +
                "\t\t\t\t\t\t\t\t";
        c.contactSearchPostcode = "ba22 8rt";
        c.password = "12345678";
        c.retypePassword = "12345678";
    });
}
