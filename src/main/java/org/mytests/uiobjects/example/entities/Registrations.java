package org.mytests.uiobjects.example.entities;


import com.jdiai.tools.DataClass;

public class Registrations extends DataClass<Registrations> {
    public String email, newTitle, firstName, lastName, profession,
            contactSearchPostcode, contactAddressResultSelect,
            password, retypePassword;

}
